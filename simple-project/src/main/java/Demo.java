import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;

public class Demo {

    public static void main(String[] args) {
        String pathToChromeDriver = "C:\\chrome\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", pathToChromeDriver);

        WebDriver driver = new ChromeDriver();

        driver.navigate().to("https://www.youtube.com/");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> titles = driver.findElements(By.xpath("//a[@id='video-title']"))
                .stream().map(WebElement::getText).collect(Collectors.toList());

        for (String title : titles) {
            System.out.println(title);
        }
    }

}
